var expect = require('expect');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var server = require("../bin/www");
var monk = require('monk');
var async = require('async');
require('./helper.js');

var url = 'http://localhost:3000';

describe('Update User', function () {
	before(function (done){
		db = monk('localhost:27017/node_interview_question');
		
		// clear out anything leftover from previous tests
		var collection = db.get('userlist');
		collection.remove();

		var newUsers = [
		{ 
			'username': 'test user' 
		}
		];
		
		// adds test users
		async.each(newUsers, function(user, callback){
			addUser(db, user, callback);
		}, done);
	});

	it('updates user \'test user\' to have a new email', function (done) {
		var updatedUser = {
			'username' : 'test user',
			'email' : 'test3@test.com', //new email!
			'fullname' : 'Bob Smith',
			'age' : 27,
			'location' : 'San Francisco',
			'gender' : 'Male'
			};
			
		var collection = db.get('userlist');
		collection.findOne({ 'username': 'test user' }, function (err, doc) {
			var userId = doc._id;
			request(url)
				.put('/users/updateuser/' + userId)
				.send(updatedUser)
				.expect(200)
				.end(function (err, res) {
					if (err) {
						console.log(JSON.stringify(res));
						throw err;
					}
					done();
				});
		});
	});

	it('attempts to update user \'test user\' with new, incorrectly formatted email', function (done){
		addUser(db, {'username': 'test user'}, function (){
			var updatedUser = {
				'username' : 'test user',
				'email' : 'bad', // new, incorrectly formatted email
				'fullname' : 'Bob Smith',
				'age' : 27,
				'location' : 'San Francisco',
				'gender' : 'Male'
				};
				
			var collection = db.get('userlist');
			collection.findOne({ 'username': 'test user' }, function (err, doc) {
				var userId = doc._id;

				var badEmails = ['bad', 'almost@email', 'email.com'];

				// attempt to update the user with each bad email address
				async.each(badEmails, function (email, callback){
					request(url)
					.put('/users/updateuser/' + userId)
					.send(updatedUser)
					.expect(400, { msg: 'error: invalid user info'})
					.end(function (err, res) {
						if (err) {
							console.log(JSON.stringify(res));
							throw err;
						}
						callback();
					});
				}, done);
			});
		});
	});

	it('attempts to update a user that doesn\'t exist', function (done){
		var userId = monk.id();
		var updatedUser = {'email': 'updated@email.com'};
		var collection = db.get('userlist');

		// make *sure* user doesn't already exist
		collection.count({ '_id': userId }, function (err, count){
			if (err)
				throw err;
			expect(count).toEqual(0);
		});

		request(url)
		.put('/users/updateuser/' + userId)
		.send(updatedUser)
		.expect(400)
		.end(function (err, res){
			if (err) {
				console.log(JSON.stringify(res));
				throw err;
			}
			expect(res.body).toEqual({msg: 'error: user doesn\'t exist'});
			done();
		});
	});

	after(function (done){
		db.close();
		done();
	});
});