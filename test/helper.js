objectAssign = require('object-assign');

addUser = function(db, user, callback){
	// generic test user
	var testUser = {
		'username' : 'test user',
		'email' : 'test1@test.com',
		'fullname' : 'Bob Smith',
		'age' : 27,
		'location' : 'San Francisco',
		'gender' : 'Male'
		};
	// add/overwrite any properties passed in
	objectAssign(testUser, user);

	var collection = db.get('userlist');
	collection.insert(testUser, function (err, res){
		if (err){
			throw err;
			console.log(err);
		}

		callback();
	});
};

removeId = function (object){
	if ('_id' in object){
		var objWithoutId = objectAssign({}, object);
		delete objWithoutId['_id'];
		return objWithoutId;
	}else{
		return object;
	}
};