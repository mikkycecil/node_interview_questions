var expect = require('expect');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var server = require("../bin/www");
var monk = require('monk');
var objectAssign = require('object-assign');
var async = require('async');
var forEach = require('foreach');
require('./helper.js');

var url = 'http://localhost:3000';
var db;

describe('Add User', function(){
	before(function (done){
		db = monk('localhost:27017/node_interview_question');
		
		// clear out anything leftover from previous tests
		var collection = db.get('userlist');
		collection.remove();

		done();
	});

	it('Adds a new user with user name \'test user\'', function (done){
		var newUser = {
			'username' : 'test user',
			'email' : 'test1@test.com',
			'fullname' : 'Bob Smith',
			'age' : 27,
			'location' : 'San Francisco',
			'gender' : 'Male'
			};
		
		request(url)
		.post('/users/adduser')
			.send(newUser)
			.expect(201) // TODO: add location header
			.expect(function (res){
				// check that it returns the new object with an assigned id
				expect(res.body).toBeA('object');
				expect(res.body).toIncludeKey('_id');
				expect(removeId(res.body)).toEqual(newUser);
				// check that it returns the location of the new user
				expect(res.header).toIncludeKey('location');
				expect(res.header.location).toInclude('/user/');
			})
			.end(function(err, res){
				if(err){
					console.log(JSON.stringify(res));
					throw err;
				}

				done();
			});
	});

	it('Attempts to add a new user with incorrectly formatted email', function (done){
		var userList = [];
		var newUser = {
			'username' : 'test user',
			'fullname' : 'Bob Smith',
			'age' : 27,
			'location' : 'San Francisco',
			'gender' : 'Male'
			};

		var badEmails = ['bad', 'almost@email', 'email.com'];

		// attempt to create a user with each bad email addressg
		async.each(badEmails, function (email, callback){
			newUser['email'] = email;
			request(url)
				.post('/users/adduser')
				.send(newUser)
				.expect(400, { msg: 'error: invalid user info'})
				.end(function (err, res){
					if (err){
						console.log(JSON.stringify(res));
						throw err;
					}
					callback();
				});
		}, done);
	});

	after(function (done){
		db.close();
		done();
	});
});