var expect = require('expect');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var server = require("../bin/www");
var monk = require('monk');
var async = require('async');
require('./helper.js');

var url = 'http://localhost:3000';

describe('Delete User', function () {
	before(function (done){
		db = monk('localhost:27017/node_interview_question');
		console.log('Adding user with username \'test user\' to the database');
		
		var newUsers = [
		{ 
			'username': 'test user' 
		}
		];

		// adds test users
		async.each(newUsers, function(user, callback){
			addUser(db, user, callback);
		}, done);
	});

	it('deletes the \'test user\' from the database', function (done) {
		var collection = db.get('userlist');
		collection.findOne({ 'username': 'test user' }, function (err, doc) {
			if (err)
				throw err;

			var userId = doc._id;
			request(url)
				.delete('/users/deleteuser/' + userId)
				.expect(200)
				.end(function (err, res) {
					if (err) {
						console.log(JSON.stringify(res));
						throw err;
					}
					done();
				});
		});
	});

	it('attempts to delete a user that doesn\'t exist from the database', function (done) {
		var collection = db.get('userlist');
		var userId = monk.id();

		// make *sure* user doesn't already exist
		collection.count({ '_id': userId }, function (err, count){
			if (err)
				throw err;
			expect(count).toEqual(0);
		});

		request(url)
			.delete('/users/deleteuser/' + userId)
			.expect(400, { msg: 'error: user doesn\'t exist' })
			.end(function (err, res) {
				if (err)
					throw err;
				done();
			});
	});

	after(function (done){
		db.close();
		done();
	});
});