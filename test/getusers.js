var expect = require('expect');
var should = require('should');
var assert = require('assert');
var request = require('supertest');
var server = require("../bin/www");
var monk = require('monk');
var async = require('async');
require('./helper.js');

var url = 'http://localhost:3000';

describe('Get Users', function(){
	before(function (done){
		db = monk('localhost:27017/node_interview_question');

		// clear out anything leftover from previous tests
		var collection = db.get('userlist');
		collection.remove();

		var newUsers = [
		{
			'username' : 'one user',
			'email' : 'test2@test.com',
			'fullname' : 'Helen Smith',
			'age' : 27,
			'location' : 'Los Angeles',
			'gender' : 'Female'
		},
		{}
		];

		// adds test users
		async.each(newUsers, function(user, callback){
			addUser(db, user, callback);
		}, done);
	});

	it('Gets all users', function (done){
		request(url)
		.get('/users/userlist')
		.expect(200)
		.end(function (err, res){
			if(err){
				console.log(JSON.stringify(res));
				throw err;
			}
			var users = JSON.parse(res.text);
			expect(users.length).toEqual(2);
			done();
		});
	});

	it('Gets one user', function (done){
		var collection = db.get('userlist');
		collection.findOne({ 'username': 'one user' }, function (err, doc) {
			if (err){
				throw err;
			}
			expect(doc)
			.toExist()
			.toIncludeKey('_id');

			var newUser = doc;
			var userId = doc._id;
			request(url)
			.get('/users/user/' + userId)
			.expect(200)
			.end(function (err, res){
				if(err){
					console.log(JSON.stringify(res));
					throw err;
				}
				expect(res.body).toBeA('object');
				expect(removeId(res.body)).toEqual(removeId(newUser));
				done();
			});
		});
	});

	after(function (done){
		db.close();
		done();
	});
});