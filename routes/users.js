var express = require('express');
var router = express.Router();
var forEach = require('foreach');

isEmailValid = function(email){
    var alphaNum = '([a-z]|[A-Z]|[0-9]|_|-|\.)';
    var emailRegex = new RegExp(alphaNum + '+@' + alphaNum +'+\\.' + alphaNum + '+');
    return emailRegex.test(email);
};

isUserInfoValid = function(user){
    // check required fields
    var requiredFields = ['username', 'email'];
    forEach(requiredFields, function (field){
        if (!(field in user)){
            return false;
        }
    });
    
    // validate email field
    if (!isEmailValid(user.email)){
        return false;
    }

    return true;
};

isUserUpdateInfoValid = function(user){
    // validate email field if it's being updated
    if (('email' in user) && !isEmailValid(user.email)){
        return false;
    }

    return true;
};

/*
 * GET userlist.
 */
router.get('/userlist', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    collection.find({},{},function(e,docs){
        res.json(docs);
    });
});

/*
 * GET one user.
 */
router.get('/user/:id', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    var userToGet = req.params.id;
    collection.findOne({ '_id': userToGet }, function(err, docs){
        if (err){
            res.status(500).send({ msg: 'error: ' + err });
        }else{
            res.status(200).send(docs);
        }
    });

});

/*
 * POST to adduser.
 */
router.post('/adduser', function(req, res) {
    if (isUserInfoValid(req.body)){
        var db = req.db;
        var collection = db.get('userlist');
        collection.insert(req.body, function(err, result){
            if (err){
                res.status(500).send({ msg: 'error: ' + err });
            }else{
                res.status(201).location('/user/' + result._id).send(result);
            }
        });
    }else{
        res.status(400).send({ msg: 'error: invalid user info'});
    }
});

/*
 * DELETE to deleteuser.
 */
router.delete('/deleteuser/:id', function(req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    var userToDelete = req.params.id;
    
    collection.remove({ '_id' : userToDelete }, function(err, doc) {
        console.log(doc.deletedCount);
        if (err){
            res.status(500).send({ msg: 'error: ' + err });
        }else if (doc.deletedCount == 0){
            res.status(400).send({ msg: 'error: user doesn\'t exist' });
        }else{
            res.status(200).send({});
        }
    });
});

/*
 * PUT to updateuser.
 */
router.put('/updateuser/:id', function(req, res) {
    if (isUserUpdateInfoValid(req.body)){
        var db = req.db;
        var collection = db.get('userlist');
        var userToUpdate = req.params.id;
    
        collection.findOneAndUpdate({ '_id': userToUpdate }, req.body, function (err, doc){
            if (err){
                res.send({ msg: 'error: ' + err });
            }else if (doc === null){
                res.status(400).send({ msg: 'error: user doesn\'t exist' });
            }else{
                res.status(200).send(doc);
            }
        });    
    }else{
        res.status(400).send({ msg: 'error: invalid user info'});
    }
});


module.exports = router;